const con = require('../config/database')
const {validationResult} = require('express-validator')
const WK = require('../utility/const')
const { sendMessage } = require('../utility/io');

const table="workflows"

module.exports={
    get:(req,res)=>{
        con.query("SELECT * FROM "+table+" ORDER BY id DESC",function(err,rows){
            if(err){
                return res.status(500).json({
                    status:false,
                    message:"Internal server error"
                })
            }else{
                return res.status(200).json({
                    status:true,
                    message:"Data Ok",
                    data:rows
    
                })
            }
        })
    },
    save:(req,res)=>{
        const error = validationResult(req)
        if(!error.isEmpty()){
            return res.status(422).json({
                errors:error.array()
            })
        }

        let formData={
            userid                  : req.body.userid,
            connect_id                  : req.body.connect_id,
            name                    : req.body.name,
            publish_mode             : req.body.publish_mode,
            status                  : req.body.status,
            source_type              :   req.body.source_type,
            source_media_type         :   req.body.source_media_type,
            source_connection        :   req.body.source_connection,
            source_playlist          :   req.body.source_playlist,
            source_video_type         :   req.body.source_video_type,
            source_folder            :   req.body.source_folder,
            source_action            :   req.body.source_action,
            destination_connection   :   req.body.destination_connection,
            destination_playlist     :   req.body.destination_playlist,
            destination_folder       :   req.body.destination_folder
        }

        con.query("INSERT INTO "+table+" SET ? ",formData,function(err,rows){
            if(err){
                console.log(err)
                return res.status(500).json({
                    status: false,
                    message:    "Internal Server"
                })
            }else{
                sendMessage('client', {name:'',message:'save workflows Ok'});

                return res.status(201).json({
                    status:true,
                    message:"Insert Ok",
                    data:req.body
                })
            }
        })
        
    },
    delete:(req,res)=>{
        const error = validationResult(req)
        if(!error.isEmpty()){
            return res.status(422).json({
                errors:error.array()
            })
        }

        con.query("DELETE FROM "+table+" WHERE id='"+req.body.id+"' ",function(err,rows){
            if(err){
                return res.status(500).json({
                    status: false,
                    message:    "Internal Server"
                })
            }else{
                return res.status(200).json({
                    status:true,
                    message:"Delete Ok",
                    data:req.body
                })
            }
        })
        
    }

}
