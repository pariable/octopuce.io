const con = require('../config/database')
const {validationResult} = require('express-validator')
const table="templates"

module.exports={
    get:(req,res)=>{
        con.query("SELECT * FROM "+table+" ORDER BY id",function(err,rows){
            if(err){
                return res.status(500).json({
                    status:false,
                    message:"Internal server error"
                })
            }else{
                return res.status(200).json({
                    status:true,
                    message:"Data Ok",
                    data:rows
    
                })
            }
        })
    },
    save:(req,res)=>{
        const error = validationResult(req)
        if(!error.isEmpty()){
            return res.status(422).json({
                errors:error.array()
            })
        }

        let formData={
            userid : req.body.userid,
            name    : req.body.name
        }

        con.query("INSERT INTO "+table+" SET ? ",formData,function(err,rows){
            if(err){
                return res.status(500).json({
                    status: false,
                    message:    "Internal Server"
                })
            }else{
                return res.status(201).json({
                    result:true,
                    message:"Insert Ok",
                    data:req.body
                })
            }
        })

    },
    delete:(req,res)=>{
        const error = validationResult(req)
        if(!error.isEmpty()){
            return res.status(422).json({
                errors:error.array()
            })
        }

        con.query("DELETE FROM "+table+" WHERE id='"+req.body.id+"' ",function(err,rows){
            if(err){
                return res.status(500).json({
                    status: false,
                    message:    "Internal Server"
                })
            }else{
                return res.status(200).json({
                    result:true,
                    message:"Delete Ok",
                    data:rows[0]
                })
            }
        })

    }
}