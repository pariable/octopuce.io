const CryptoJS = require('crypto-js');  

module.exports.encrypt=(text)=>{
    const encodedWord = CryptoJS.enc.Utf8.parse(text); // encodedWord Array object
    const encoded = CryptoJS.enc.Base64.stringify(encodedWord); // string: 'NzUzMjI1NDE='
    return encoded;
}

module.exports.decrypt=(text)=>{
    const encodedWord = CryptoJS.enc.Base64.parse(text); // encodedWord via Base64.parse()
    const decoded = CryptoJS.enc.Utf8.stringify(encodedWord); // decode encodedWord via Utf8.stringify() '75322541'
    return decoded;
}

