/*
+------------+--------------+------+-----+-------------------+-------------------+
| Field      | Type         | Null | Key | Default           | Extra             |
+------------+--------------+------+-----+-------------------+-------------------+
| id         | int          | NO   | PRI | NULL              | auto_increment    |
| UserId     | int          | YES  | MUL | NULL              |                   |
| ConnectId  | int          | YES  | MUL | NULL              |                   |
| link       | varchar(255) | YES  |     | NULL              |                   |
| ExpireDate | datetime     | YES  |     | NULL              |                   |
| status     | int          | YES  |     | NULL              |                   |
| CreatedAt  | timestamp    | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
| UpdatedAt  | timestamp    | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
+------------+--------------+------+-----+-------------------+-------------------+
*/
const express = require('express')
const router = express.Router()
const con = require('../config/database')
const {body,validationResult} = require('express-validator')
const controller=require('../controller/connectionsController')

router.get('/',controller.get)

//post
router.post('/save',[
    body('userid').notEmpty(),
    body('connect_m_id').notEmpty(),
    body('name').notEmpty(),
    body('access_url').notEmpty(),
    body('access_token').notEmpty(),
    body('expire_date').notEmpty(),  
    body('status').notEmpty()
],controller.save)


//delete
router.delete('/delete',[
    body('id').notEmpty()
],controller.delete)


module.exports=router

