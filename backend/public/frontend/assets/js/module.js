
$(function(){

    //////////////////////// INDEX ////////////////////////////
    let Index={
        init    :   ()=>{
            $('#frmLogin').validate({
                submitHandler: function(form) {
                    req={
                    email : $('#txtLoginEmail').val(),
                    password : $('#txtLoginPassword').val()
        
                    }
        
                    onProcess('frmLogin','Loading ...')
                    Ajax(Api.Users.Login.URL,Api.Users.Login.Method,req,function(){
                        endProcess('frmLogin','Login')
                        
                        if(res.status){
                            //storage
                            setSession();
                            socket.emit('client', 
                                {
                                    name:req.email,
                                    email:req.email,
                                    message:MESSAGE.LOGIN_SUCCESS
                                }
                            );

                            window.location.href=ACCOUNT_URL;

                        }else{
                            $('#txtLoginEmail').val('').focus();
                            $('#txtLoginPassword').val('')
                            alert("Invalid Account!")
                        }
                    
                    })
                }
            });
    
            $('#frmRegister').validate({
                submitHandler: function(form) {
                    req={
                    name      : $('#txtRegisterName').val(),
                    phone     : $('#txtRegisterPhone').val(),
                    email     : $('#txtRegisterEmail').val(),
                    password  : $('#txtRegisterPassword').val()
                    }
        
                    onProcess('frmRegister','Loading ...')
                    Ajax(Api.Users.register.URL,Api.Users.register.Method,req,function(){
                        endProcess('frmRegister','Register')
                        console.log(res);
                        
                        if(res.status){
                            $('#viewModalDemo').modal();
                            $('.vLogin').hide();
                            $('.vRegister').hide();
                            $('.vPass').hide();
                            $('.vConfirmRegister').show();
                            $('.vConfirmPass').hide();

                            setTimeout(function(){
                                $('.actLogin').trigger('click')
                            },'5000')
                            
                        }else{
                            $('#txtRegisterEmail').val('').focus()
                            notify('Email has been registered','error')
                            //alert("Email has been used!")
                        }
                    
                    })
                }
            });
    
            $('#frmForgot').validate({
                submitHandler: function(form) {
                    req={
                        email : $('#txtForgotEmail').val()
                    }
        
                    onProcess('frmForgot','Loading ...')
                    Ajax(Api.Users.Forgot.URL,Api.Users.Forgot.Method,req,function(){
                        endProcess('frmForgot','Send Link')
                        console.log(res);
                        
                        if(res.status){
                            $('#viewModalDemo').modal();
                            $('.vLogin').hide();
                            $('.vRegister').hide();
                            $('.vPass').hide();
                            $('.vConfirmRegister').hide();
                            $('.vConfirmPass').show();
                            $('#txtForgotEmail').val('')
                            
                        }else{
                            $('#txtForgotEmail').val('').focus();
                            alert("Invalid Account!")
                        }
                    
                    })
                    
                }
            });
        }
    }

    /////////////////////// CONNECTIONS ////////////////////////////
    let Connections={
        default:()=>{
            //default
            
            $('input').val('');
        },
        init    :  ()=>{
           
            Connections.default();

            $('.btnAddConnections').on('click',function(){

                Ajax(Api.ConnectionsMaster.Get.URL,Api.ConnectionsMaster.Get.Method,req,function(){
                    
                    console.log(res);
                    
                    if(res.status){
                        let el="";
                        if(res.data.length > 0){
                            $.each(res.data,function(i,e){
                               let contype=0;

                               if(e.connection_type==1){
                                    contype="Source"
                               }
                               if(e.connection_type==2){
                                contype="Destination"
                                }
                                if(e.connection_type==3){
                                    contype="Source / Destination"
                            }

                                el+="<div class='col divGallery'>";
                                    el+="<div class='gallery galleryConnection' data-id='"+e.id+"' data-name='"+e.name+"' data-label='"+e.label+"' data-link_image='"+e.link_image+"' data-contype='"+e.connection_type+"'>";
                                        el+="<a  class='conLists' target='#' href='#'>";
                                            el+="<img src='/frontend/assets/images/icons/"+e.link_image+"' alt='Cinque Terre' width='600' height='400'>";
                                        el+="</a>";
                                        el+="<div class='desc'>"+e.label+"<br><span >"+contype+"</span></div>";
                                        
                                    el+="</div>";
                                el+="</div>";
                                
                            })
                        }

                        $('#modalAddConnection').modal('show',{backdrop: 'static', keyboard: false});

                        render('#contentConnectionLists',el,()=>{
                            $('.text-modal').text('');
                            $('.img-modal' ).hide()
                            $('.galleryConnection').each((i,e)=>{
                                $(e).click(()=>{
                                   
                                    Component.Render(Api.Connections.Components($(e).data('name')),'GET',()=>{
                                        render('#contentConnectionLists',res,()=>{
                                            
                                            $('.img-modal' ).prop("src","/frontend/assets/images/icons/"+$(e).data('link_image')).show()
                                           $('.text-modal').text($(e).data('label'))
                                            $('.btnRetryConnection').off().on('click',function(){
                                                
                                                $('.btnAddConnections').trigger('click')
                                                return false;
                                            })

                                            $('.btnConnect').on('click',function(){
                                                notify('Wow!! ...Directing to your Connections','success')
                                                $('#modalAddConnection').modal('hide')
                                                return false;
                                            })
                                        
                                        })
                                    })
                                    return false;
    
                                })
                            })
                            
                        })
                        
                    }else{
                        showAlert('#contentConnectionLists','warning','Empty Record.')
                    }
                
                })
                
            })

            $('.btnRefreshWorkflow').on('click',function(){
                
            })
        }
    }

    /////////////////////// WORKFLOWS ////////////////////////////
    let Workflows={
        default:()=>{
            //default
            $('#divFormYoutube').hide();
            $('#divFormGoogleDrive').hide();
            $('#worflow-table').show();
            $('input').val('');
        },
        init    :  ()=>{
           
            Workflows.default();

            $('.btnAddWorkflow').on('click',function(){
                $('#workflow-table').hide();

                $("#wizard")
                    .steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft",
                        enableAllSteps:false,
                        enableContentCache:true,
                        saveState:false,    
                        onStepChanging:function (event, currentIndex, newIndex) {
                            
                            $('#optFormWorkflowSourceType').change(function(){
                                var val=$(this).val();
                                
                                if(val=="gdrive"){
                                    
                                    $('#divFormGoogleDrive').show();
                                    $('#divFormYoutube').hide();
    
                                }
    
                                if(val=="youtube"){
                                    $('#divFormGoogleDrive').hide();
                                    $('#divFormYoutube').show();
    
                                }

                            })

                            return true; 
                        },// Fires before the step changes and can be used to prevent step changing by returning `false`.
                        onStepChanged:function (event, currentIndex, priorIndex) {
                           
                            let name=$('#txtWorkflowName').val();
                            if(name==''){
                                notify('Please fill the Workflow Name!','error')
                                $('.actions > ul > li:nth-child(2) > a').attr('href','#next');
                                return false;   
                            }
                            
                         },// Fires after the step has change.
                        onFinishing:function (event, currentIndex) {return true; },// Fires before finishing and can be used to prevent completion by returning `false`.
                        onFinished:function (event, currentIndex) { },
                    })
                    .show();
        
            })

            $('.btnRefreshWorkflow').on('click',function(){
                
                
            })

            
        }
    }

    /////////////////////// ALL PAGE EXCEPT INDEX ////////////////////////////
    let GlobalPage={
        init    :   ()=>{
            if(typeof(getSession.name) !=='string'){
                window.location.href=BASE_PAGE;
            }
      
            $('.initial').text(Array.from(getSession.name)[0].toUpperCase())
            $('.title-content-account').text(getSession.name).css('textTransform', 'capitalize');
            $('.account-name').text(getSession.name).css('textTransform', 'capitalize');
            $('.account-email').text(getSession.email)
            $('.account-phone').text(getSession.phone)
            const join_date=getSession.createdat.split(' ');
    
            $('.join_date').text(join_date[0]+" "+join_date[1]+" "+join_date[2]+" "+join_date[3])
            $('.btnLogout').on('click',function(e){
                removeSession();
                window.location.href=BASE_PAGE;
                return false;
            })

            $('#tblWorkflow').dataTable({
                pageLength: 15,
                searching: true,
                ordering:  true
                  
            });
        
            $('#tblConnection').dataTable({
            
            });
        
            $('#tblTemplate').dataTable({
            
            });

        }
    }

    /////////////////////// DO EXECUTION ////////////////////////////

    if(page=="index"){
        Index.init();
    }

    if(page=='workflows'){
        Workflows.init();
    }

    if(page=='connections'){
        Connections.init();
    }

    if(page !='index'){
        GlobalPage.init();
    }
})