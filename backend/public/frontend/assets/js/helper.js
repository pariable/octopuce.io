function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

function onProcess(o,t){
    $("#"+o).find('button').attr('disabled','disabled').text(t)
}

function endProcess(o,t){
    $("#"+o).find('button').removeAttr('disabled').text(t)
}

const getDateFromString = str => {
    const date = str.split("T")[0];
    const time = str.split(".")[0].split('T')[1];
    str = date+" "+time
    return new Date(str);
}
function ucwords(s){
    var str = s.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
}

function setSession(){
    localStorage.setItem('id',res.data.id);
    localStorage.setItem('name',res.data.name);
    localStorage.setItem('phone',res.data.phone);
    localStorage.setItem('email',res.data.email);
    localStorage.setItem('createdat',getDateFromString(res.data.CreatedAt));

}

function removeSession(){
    localStorage.removeItem('id');
    localStorage.removeItem('name');
    localStorage.removeItem('phone');
    localStorage.removeItem('email');
    localStorage.removeItem('createdat');
}

var getSession={
    "name"  : localStorage.getItem('name'),
    "phone"  : localStorage.getItem('phone'),
    "email"  : localStorage.getItem('email'),
    "createdat"  : localStorage.getItem('createdat')
}

function notify(t,c){
    
    $.notify(t,{
        // whether to hide the notification on click
        clickToHide: true,
        // whether to auto-hide the notification
        autoHide: true,
        // if autoHide, hide after milliseconds
        autoHideDelay: 5000,
        // show the arrow pointing at the element
        arrowShow: true,
        // arrow size in pixels
        arrowSize: 5,
        // position defines the notification position though uses the defaults below
        position: 'top right',
        // default positions
        elementPosition: 'top right',
        globalPosition: 'top right',
        // default style
        //bootstrap or custom see addStyle
        style: 'bootstrap',
        // default class (string or [string])
        // error success info warn
        className: c,
        // show animation
        showAnimation: 'slideDown',
        // show animation duration
        showDuration: 400,
        // hide animation
        hideAnimation: 'slideUp',
        // hide animation duration
        hideDuration: 200,
        // padding between element and notification
        gap: 2
    })
    
}

function render(o,el,f){
    $(o).empty().html(el)
    f()
}

function showAlert(o,t,s){
    $(o).empty().html("<div class='alert alert-"+t+" role='alert'>"+s+"</div>");
}