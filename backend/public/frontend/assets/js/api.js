

var socket = io.connect(':3000');
const Api={
    Users:  {
        Login       :   {
            URL     :    BASE_API+"users/login",
            Method  :   "POST"
        },
        register       :   {
            URL     :    BASE_API+"users/save",
            Method  :   "POST"
        },
        Forgot       :   {
            URL     :    BASE_API+"users/forgot",
            Method  :   "POST"
        }
    },
    Connections     :  {
        Save        :   {
            URL     :    BASE_API+"connections/save",
            Method  :   "POST"
        },
        Get         :   {
            URL     :    BASE_API+"connections",
            Method  :   "GET"
        },
        Delete      :   {
            URL     :    BASE_API+"connections/delete",
            Method  :   "DELETE"
        },
        Components  : (obj)=>{
           return HOST+"connections/"+obj;
        }
    },
    ConnectionsMaster     :  {
        Save        :   {
            URL     :    BASE_API+"master_connections/save",
            Method  :   "POST"
        },
        Get         :   {
            URL     :    BASE_API+"master_connections",
            Method  :   "GET"
        },
        Delete      :   {
            URL     :    BASE_API+"master_connections/delete",
            Method  :   "DELETE"
        }
    },
    Workflows       :   {
        Save        :   {
            URL     :    BASE_API+"workflows/save",
            Method  :   "POST"
        },
        Get         :   {
            URL     :    BASE_API+"workflows",
            Method  :   "GET"
        },
        Delete       :   {
            URL     :    BASE_API+"workflows/delete",
            Method  :   "DELETE"
        }
    },
    Templates   :   {
        Save        :   {
            URL     :    BASE_API+"templates/save",
            Method  :   "POST"
        },
        Get         :   {
            URL     :    BASE_API+"templates",
            Method  :   "GET"
        },
        Delete       :   {
            URL     :    BASE_API+"templates/delete",
            Method  :   "DELETE"
        }

    },
    Calendars    :   {
        Save        :   {
            URL     :    BASE_API+"calendars/save",
            Method  :   "POST"
        },
        Get         :   {
            URL     :    BASE_API+"calendars",
            Method  :   "GET"
        },
        Delete       :   {
            URL     :    BASE_API+"calendars/delete",
            Method  :   "DELETE"
        }
    }
}

const Component={
    'Render'    :   (url,type,e)=>{

       
        $.ajax({
            headers     : {
                'Accept'        : 'application/json',
                'Content-Type'  : 'application/json'
            },
            xhrFields   :  'withCredentials:true',
            url         :   url,
            crossOrigin :   true,
            type        :   type,
            data        :   {},
            dataType    :   "html",
            success: function(data) {
                console.log(data)
                res=data;
                e();
            },
            error: function(data) {
                console.log(data)
                if(data.status==500){
                    showAlert('#contentConnectionLists','warning','Empty Record.')
                }
                
                e();
            },
            
        })

    }
}

var res=[];
var req={};

function clearRequest(){
    req={}
}

let Ajax=function(url,type,datas,e){
    $.ajax({
        headers     : {
            'Accept'        : 'application/json',
            'Content-Type'  : 'application/json'
        },
        xhrFields   :  'withCredentials:true',
        url         :   url,
        crossOrigin :   true,
        type        :   type,
        data        :   JSON.stringify(datas),
        dataType    :   "json",
        success: function(data) {
            res=data;
            e();
        },
        error: function(data) {
            res=data.responseJSON;
            //notify(MESSAGE.DB_ERROR,'error')
            e();
        },
        
    })
}